/*
 * Copyright 2015-2019 Autoware Foundation. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef PURE_PURSUIT_PURE_PURSUIT_CORE_H
#define PURE_PURSUIT_PURE_PURSUIT_CORE_H

#include <ros/ros.h>
#include <std_msgs/Float32.h>
#include <gps_common/GPSFix.h>
#include <sensor_msgs/Imu.h>
#include <sensor_msgs/PointCloud2.h>

using namespace std;

namespace TopicRelay 
{

class TopicRelayNode

{
public:
  TopicRelayNode();
  ~TopicRelayNode();

  void run();
  // friend class PurePursuitNodeTestSuite;

private:
  // handle
  ros::NodeHandle nh_;
  ros::NodeHandle private_nh_;

  // publisher
  ros::Publisher pub1_, pub2_;

  // subscriber
  ros::Subscriber sub1_, sub2_;

  // constant
  // const int LOOP_RATE_;  // processing frequency
  // const std::string MAP_FRAME_;  // processing frequency
  // const std::string GPS_FRAME_;  // processing frequency
  

  std::string input_imu_topic_,  output_imu_topic_,    output_imu_frame_id_; 
  std::string input_lidar_topic_,output_lidar_topic_ , output_lidar_frame_id_;

  // double localcartesian_lon0_;
  // double localcartesian_h0_;
  // double localcartesian_yaw_;

  // initializer
  void initForROS();


  // callbacks
  void callbackFromIMU( const sensor_msgs::Imu& imu);
  // void callbackFromLidar( const sensor_msgs::PointCloud2& points);

};

} // namespace waypoint_follower

#endif  // PURE_PURSUIT_PURE_PURSUIT_CORE_H

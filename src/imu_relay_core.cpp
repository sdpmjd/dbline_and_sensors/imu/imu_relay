/*
 * Copyright 2015-2019 Autoware Foundation. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <vector>
#include <topic_relay/topic_relay.h>

namespace TopicRelay
{
TopicRelayNode::TopicRelayNode()
  : private_nh_("~")
  // , LOOP_RATE_( 10000/*30*/)
  // ,MAP_FRAME_("gnss")
  // ,GPS_FRAME_("gps")
{
  initForROS();
}

TopicRelayNode::~TopicRelayNode()
{}

void TopicRelayNode::initForROS()
{
  // ros parameter settings
  // private_nh_.param( "velocity_source", velocity_source_, 0);
  // private_nh_.param( "publishes_for_steering_robot", publishes_for_steering_robot_, false);
  // private_nh_.param( "minimum_lookahead_distance", minimum_lookahead_distance_, 6.0);

  nh_.param<std::string>("input_imu_topic",     input_imu_topic_, "/IMU_data");
  nh_.param<std::string>("output_imu_topic",    output_imu_topic_, "/imu_raw");
  nh_.param<std::string>("output_imu_frame_id", output_imu_frame_id_, "imu-link");

  // nh_.param<std::string>("input_lidar_topic",     input_lidar_topic_, "/rslidar_points");
  // nh_.param<std::string>("output_lidar_topic",    output_lidar_topic_, "/points_raw");
  // nh_.param<std::string>("output_lidar_frame_id", output_lidar_frame_id_, "velodyne");
  // ROS_INFO_STREAM(pose_ndt_gnss);

  // setup subscriber
  sub1_ = nh_.subscribe(input_imu_topic_, 1, &TopicRelayNode::callbackFromIMU, this);
  // sub2_ = nh_.subscribe(input_lidar_topic_, 1, &TopicRelayNode::callbackFromLidar, this);
  pub1_ = nh_.advertise<sensor_msgs::Imu>("/imu_raw", 10);
  // pub2_ = nh_.advertise<sensor_msgs::PointCloud2>("/points_raw", 10);
}

void TopicRelayNode::callbackFromIMU( const sensor_msgs::Imu& imu)
{
  sensor_msgs::Imu new_imu = imu; 
  new_imu.header.frame_id="imu-link";
  pub1_.publish(new_imu);
}
#if 0
void TopicRelayNode::callbackFromLidar( const sensor_msgs::PointCloud2& points)
{

  sensor_msgs::PointCloud2 new_points = points; 
  new_points.header.frame_id="velodyne";
  pub2_.publish(new_points);
}
#endif

void TopicRelayNode::run()
{

#if 1
  ros::spin();
  return;
#else
  ros::Rate loop_rate(LOOP_RATE_);
  while (ros::ok()) {
    try {
        ros::spinOnce();
    }catch (const exception& e){
          cerr    << "Caught exception: " << e.what() << "\n";
    }
    loop_rate.sleep();
  }
#endif
}

}  // namespace waypoint_follower

